#include "log.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

namespace vl_containers
{
    #ifdef CONT_LOG$

        FILE* set_log_file( const char file_name[] )
        {
            FILE* log = fopen( file_name, "w" );
            assert( log );

            return log;
        }

        #ifndef LOG_FILE_NAME$
            FILE* CONTAINERS_LOG_FILE   = set_log_file( ".CONTAINERS_LOG"  );
        #else
            FILE* CONTAINERS_LOG_FILE   = set_log_file( LOG_FILE_NAME$ );
        #endif // LOG_FILE_NAME$

        int   LOG_TABS = -1;

        char __cont_log_msg[MAX_MSG_LEN$] = {};

        char* __make_lmsg( const char* str, const char* f )
        {
            for( int i = 0; i < MAX_MSG_LEN$; ++i )
                __cont_log_msg[i] = 0;

            sprintf( __cont_log_msg, "%s in func %s", str, f );
            return __cont_log_msg;
        }

    #endif // CONT_LOG$

    FILE* set_dump_file( const char file_name[] )
    {
        FILE* dump = fopen( file_name, "w" );
        assert( dump );

        return dump;
    }

    #ifndef TREE_DUMP_FILE_NAME$
        FILE* TREE_DUMP_FILE        = set_dump_file( ".TREE_T_DUMP" );
    #else
        FILE* TREE_DUMP_FILE        = set_dump_file( TREE_DUMP_FILE_NAME$ );
    #endif // TREE_DUMP_FILE_NAME$

    #ifndef STACK_DUMP_FILE_NAME$
        FILE* STACK_DUMP_FILE        = set_dump_file( ".STACK_T_DUMP" );
    #else
        FILE* STACK_DUMP_FILE        = set_dump_file( STACK_DUMP_FILE_NAME$ );
    #endif // TREE_DUMP_FILE_NAME$

    #ifndef LIST_DUMP_FILE_NAME$
        FILE* LIST_DUMP_FILE        = set_dump_file( ".LIST_T_DUMP" );
    #else
        FILE* LIST_DUMP_FILE        = set_dump_file( LIST_DUMP_FILE_NAME$ );
    #endif // LIST_DUMP_FILE_NAME$
}
