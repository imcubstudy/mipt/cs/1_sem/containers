#ifndef __LOG_H__
#define __LOG_H__

#include <cstdio>
#include "contlp.h"

namespace vl_containers
{
    FILE*  set_dump_file( const char file_name[] );

    extern FILE* STACK_DUMP_FILE;
    extern FILE* TREE_DUMP_FILE;
    extern FILE* LIST_DUMP_FILE;

    #ifndef MAX_TREE_NODE_DUMPS$
        #define MAX_TREE_NODE_DUMPS$ 1024
    #endif // MAX_TREE_NODE_DUMPS$

    #ifndef MAX_LIST_LINKS_DUMPS$
        #define MAX_LIST_LINKS_DUMPS$ 1024
    #endif // MAX_LIST_LINKS_DUMPS$

    #ifdef CONT_LOG$

        #ifndef MAX_MSG_LEN$
            #define MAX_MSG_LEN$ 1024
        #endif // MAX_MSG_LEN$

        extern int LOG_TABS;

        FILE* set_log_file( const char file_name[] );

        extern FILE* CONTAINERS_LOG_FILE;

        char* __make_lmsg( const char* str, const char* f );

        #define MAKE_LOGMSG( str ) __make_lmsg( str, __func__ )

        #define PRINT_LOG( msg )                                                \
                do                                                              \
                {                                                               \
                    for( int i = 0; i < LOG_TABS && LOG_TABS > 0; ++i )         \
                        fprintf( CONTAINERS_LOG_FILE, "\t" );                   \
                    fprintf( CONTAINERS_LOG_FILE, "%s\n", msg );                \
                    fflush( CONTAINERS_LOG_FILE );                              \
                } while( 0 )

        #define NAMED_PRINT_LOG( msg, ptr )                                     \
                do                                                              \
                {                                                               \
                    char resmsg[MAX_MSG_LEN$] = {};                             \
                    sprintf( resmsg, msg  " in %s[%p]", (ptr)->_name, ptr );    \
                                                                                \
                    PRINT_LOG( MAKE_LOGMSG( resmsg ) );                         \
                } while( 0 )

        #ifdef FULL_CONT_LOG$
    
                #define LOG_ON_START                                                \
                        do                                                          \
                        {                                                           \
                            LOG_TABS++;                                             \
                            PRINT_LOG( MAKE_LOGMSG( "FL:  started work")  );        \
                        } while( 0 )
    
                #define LOG_ON_END                                                  \
                        do                                                          \
                        {                                                           \
                            PRINT_LOG( MAKE_LOGMSG( "FL: finished work")  );        \
                            LOG_TABS--;                                             \
                        } while( 0 )
    
        #else
    
                #define LOG_ON_START
    
                #define LOG_ON_END
    
        #endif // FULL_CONT_LOG$

    #else

        #define PRINT_LOG( msg )

        #define NAMED_PRINT_LOG( msg, ptr )

        #define LOG_ON_START

        #define LOG_ON_END

    #endif // CONT_LOG$
}

#endif // __LOG_H__
