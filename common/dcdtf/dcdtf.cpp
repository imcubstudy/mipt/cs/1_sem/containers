#include <string.h>
#include <cstdlib>
#include "dcdtf.h"

namespace vl_containers
{
    int undef_type_print( FILE* fstream, const void* src )
    {
        return fprintf( fstream, "Can't print value" );
    }

    //***************************************************************************
    // Code is mostly auto-generated, manually done for bool and cstring

    int print_bool             ( FILE* fstream, const void* value );
    int print_char             ( FILE* fstream, const void* value );
    int print_short            ( FILE* fstream, const void* value );
    int print_unsigned_short   ( FILE* fstream, const void* value );
    int print_int              ( FILE* fstream, const void* value );
    int print_unsigned_int     ( FILE* fstream, const void* value );
    int print_long_int         ( FILE* fstream, const void* value );
    int print_unsigned_long_int( FILE* fstream, const void* value );
    int print_float            ( FILE* fstream, const void* value );
    int print_double           ( FILE* fstream, const void* value );
    int print_cstring          ( FILE* fstream, const void* value );

    int cmp_bool             ( const void* a, const void* b );
    int cmp_char             ( const void* a, const void* b );
    int cmp_short            ( const void* a, const void* b );
    int cmp_unsigned_short   ( const void* a, const void* b );
    int cmp_int              ( const void* a, const void* b );
    int cmp_unsigned_int     ( const void* a, const void* b );
    int cmp_long_int         ( const void* a, const void* b );
    int cmp_unsigned_long_int( const void* a, const void* b );
    int cmp_float            ( const void* a, const void* b );
    int cmp_double           ( const void* a, const void* b );
    int cmp_cstring          ( const void* a, const void* b );

    int destruct_bool             ( void* ptr );
    int destruct_char             ( void* ptr );
    int destruct_short            ( void* ptr );
    int destruct_unsigned_short   ( void* ptr );
    int destruct_int              ( void* ptr );
    int destruct_unsigned_int     ( void* ptr );
    int destruct_long_int         ( void* ptr );
    int destruct_unsigned_long_int( void* ptr );
    int destruct_float            ( void* ptr );
    int destruct_double           ( void* ptr );
    int destruct_cstring          ( void* ptr );

    int copy_bool             ( void* dest, const void* src );
    int copy_char             ( void* dest, const void* src );
    int copy_short            ( void* dest, const void* src );
    int copy_unsigned_short   ( void* dest, const void* src );
    int copy_int              ( void* dest, const void* src );
    int copy_unsigned_int     ( void* dest, const void* src );
    int copy_long_int         ( void* dest, const void* src );
    int copy_unsigned_long_int( void* dest, const void* src );
    int copy_float            ( void* dest, const void* src );
    int copy_double           ( void* dest, const void* src );
    int copy_cstring          ( void* dest, const void* src );

    int print_bool             ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%s", *(bool*)value ? "true" : "false" );
    }

    int print_char             ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%c", *(char*)value );
    }

    int print_short            ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%hd", *(short*)value );
    }

    int print_unsigned_short   ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%hu", *(unsigned short*)value );
    }

    int print_int              ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%d", *(int*)value );
    }

    int print_unsigned_int     ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%u", *(unsigned*)value );
    }

    int print_long_int         ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%ld", *(long*)value );
    }

    int print_unsigned_long_int( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%lu", *(unsigned long*)value );
    }

    int print_float            ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%g", *(float*)value );
    }

    int print_double           ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%lg", *(double*)value );
    }

    int print_cstring          ( FILE* fstream, const void* value )
    {
        return fprintf( fstream, "%s", *(char**)value );
    }

    int cmp_bool             ( const void* a, const void* b )
    {
        return *(bool*)a == *(bool*)b ? 0 : 1;
    }

    int cmp_char             ( const void* a, const void* b )
    {
        return *(char*)a - *(char*)b;
    }

    int cmp_short            ( const void* a, const void* b )
    {
        return *(short*)a - *(short*)b;
    }

    int cmp_unsigned_short   ( const void* a, const void* b )
    {
        return *(unsigned short*)a - *(unsigned short*)b;
    }

    int cmp_int              ( const void* a, const void* b )
    {
        return *(int*)a - *(int*)b;
    }

    int cmp_unsigned_int     ( const void* a, const void* b )
    {
        return (int)( *(unsigned*)a - *(unsigned*)b );
    }

    int cmp_long_int         ( const void* a, const void* b )
    {
        return (int)( *(long*)a - *(long*)b );
    }

    int cmp_unsigned_long_int( const void* a, const void* b )
    {
        return (int)( *(unsigned long*)a - *(unsigned long*)b );
    }

    int cmp_float            ( const void* a, const void* b )
    {
        return (int)( *(float*)a - *(float*)b );
    }

    int cmp_double           ( const void* a, const void* b )
    {
        return (int)( *(double*)a - *(double*)b );
    }

    int cmp_cstring          ( const void* a, const void* b )
    {
        return strcmp( *(char**)a, *(char**)b );
    }

    int destruct_bool             ( void* ptr ) { return 0; }
    int destruct_char             ( void* ptr ) { return 0; }
    int destruct_short            ( void* ptr ) { return 0; }
    int destruct_unsigned_short   ( void* ptr ) { return 0; }
    int destruct_int              ( void* ptr ) { return 0; }
    int destruct_unsigned_int     ( void* ptr ) { return 0; }
    int destruct_long_int         ( void* ptr ) { return 0; }
    int destruct_unsigned_long_int( void* ptr ) { return 0; }
    int destruct_float            ( void* ptr ) { return 0; }
    int destruct_double           ( void* ptr ) { return 0; }
    int destruct_cstring          ( void* ptr )
    {
        char* *cstr = (char**)ptr;
        if( *cstr != nullptr )
        {
            free( *cstr );
            *cstr = nullptr;
        }
        return 0;
    }

    int copy_bool             ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( bool ) );
        return 0;
    }

    int copy_char             ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( char ) );
        return 0;
    }

    int copy_short            ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( short ) );
        return 0;
    }

    int copy_unsigned_short   ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( unsigned short ) );
        return 0;
    }

    int copy_int              ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( int ) );
        return 0;
    }

    int copy_unsigned_int     ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( unsigned int ) );
        return 0;
    }

    int copy_long_int         ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( long int ) );
        return 0;
    }

    int copy_unsigned_long_int( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( unsigned long int ) );
        return 0;
    }

    int copy_float            ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( float ) );
        return 0;
    }

    int copy_double           ( void* dest, const void* src )
    {
        memcpy( dest, src, sizeof( double ) );
        return 0;
    }

    int copy_cstring          ( void* dest, const void* src )
    {
        destruct_cstring( dest );

        char* *d =  (char**)dest;
        char*  s = *(char**)src;

        *d = (char*)calloc( sizeof( char ), strlen( s ) + 1 );
        strcpy( *d, s );

        return 0;
    }

    const data_funcs_t bool_default_funcs =
            { print_bool, cmp_bool, copy_bool, destruct_bool };
    const data_funcs_t char_default_funcs =
            { print_char, cmp_char, copy_char, destruct_char };
    const data_funcs_t short_default_funcs =
            { print_short, cmp_short, copy_short, destruct_short };
    const data_funcs_t unsigned_short_default_funcs =
            { print_unsigned_short, cmp_unsigned_short, copy_unsigned_short, destruct_unsigned_short };
    const data_funcs_t int_default_funcs =
            { print_int, cmp_int, copy_int, destruct_int };
    const data_funcs_t unsigned_int_default_funcs =
            { print_unsigned_int, cmp_unsigned_int, copy_unsigned_int, destruct_unsigned_int };
    const data_funcs_t long_int_default_funcs =
            { print_long_int, cmp_long_int, copy_long_int, destruct_long_int };
    const data_funcs_t unsigned_long_int_default_funcs =
            { print_unsigned_long_int, cmp_unsigned_long_int, copy_unsigned_long_int, destruct_unsigned_long_int };
    const data_funcs_t float_default_funcs =
            { print_float, cmp_float, copy_float, destruct_float };
    const data_funcs_t double_default_funcs =
            { print_double, cmp_double, copy_double, destruct_double };
    const data_funcs_t cstring_default_funcs =
            { print_cstring, cmp_cstring, copy_cstring, destruct_cstring };

    //***************************************************************************
}
