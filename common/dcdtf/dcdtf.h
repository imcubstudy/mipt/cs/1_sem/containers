#ifndef __DCDTF_H__
#define __DCDTF_H__

#include <cstdio>

namespace vl_containers
{
    typedef int (*data_print_func)( FILE*, const void* );
    typedef int (*data_comparator)( const void*, const void* );
    typedef int (*data_destructor)( void* );
    typedef int (*data_copy_func)( void*, const void* );

    struct data_funcs_t
    {
        data_print_func     print_func;
        data_comparator     comparator;
        data_copy_func      copy_func;
        data_destructor     destructor;
    };

    int undef_type_print( FILE* fstream, const void* src );

    //***************************************************************************
    // Code is mostly auto-generated, manually done for bool and cstring


    extern const data_funcs_t bool_default_funcs;
    extern const data_funcs_t char_default_funcs;
    extern const data_funcs_t short_default_funcs;
    extern const data_funcs_t unsigned_short_default_funcs;
    extern const data_funcs_t int_default_funcs;
    extern const data_funcs_t unsigned_int_default_funcs;
    extern const data_funcs_t long_int_default_funcs;
    extern const data_funcs_t unsigned_long_int_default_funcs;
    extern const data_funcs_t float_default_funcs;
    extern const data_funcs_t double_default_funcs;
    extern const data_funcs_t cstring_default_funcs;

    //***************************************************************************
}

#endif //__DCDTF_H__
