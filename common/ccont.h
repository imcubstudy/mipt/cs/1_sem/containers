#ifndef __CCONT_H__
#define __CCONT_H__

namespace vl_containers
{
    enum cont_error_t
    {
        NO_ERROR        = 0,
        BAD_ALLOC       = 1,
        BAD_ARG         = 2,
        UNDEF_NAME      = 3,
        BAD_DATA_FUNCS  = 4,
        LOSS_OF_DATA    = 5,
        STK_OVERFLOW    = 6,
        EMPTY_POP       = 7,
        CRIT            = 8
    };
}

#include "dcdtf.h"
#include "log.h"

#define PRECONDITION_P( pointer, at_exit_code )                                         \
        do                                                                              \
        {                                                                               \
            if( (pointer) == nullptr )                                                  \
            {                                                                           \
                PRINT_LOG( MAKE_LOGMSG( "ERROR: BAD_ARG: "#pointer" is nullptr" ) );    \
                {at_exit_code}                                                          \
            }                                                                           \
        } while( 0 )

#define CONDITION_P( pointer, msg, at_exit_code )                                       \
        do                                                                              \
        {                                                                               \
            if( (pointer) == nullptr )                                                  \
            {                                                                           \
                PRINT_LOG( MAKE_LOGMSG( msg" "#pointer" is nullptr" ) );                \
                {at_exit_code}                                                          \
            }                                                                           \
        } while( 0 )

#define PRECONDITION( cond, at_exit_code )                                              \
        do                                                                              \
        {                                                                               \
            if( !(cond) )                                                               \
            {                                                                           \
                PRINT_LOG( MAKE_LOGMSG( "ERROR: BAD_ARG: "#cond" failed" ) );           \
                {at_exit_code}                                                          \
            }                                                                           \
        } while( 0 )

#define CONDITION( cond, msg, at_exit_code )                                            \
        do                                                                              \
        {                                                                               \
            if( !(cond) )                                                               \
            {                                                                           \
                PRINT_LOG( MAKE_LOGMSG( msg" "#cond" failed" ) );                       \
                {at_exit_code}                                                          \
            }                                                                           \
        } while( 0 )

#define POST_ALLOC_CHECK( pointer, at_exit_code )                                           \
        do                                                                                  \
        {                                                                                   \
            if( (pointer) == nullptr )                                                      \
            {                                                                               \
                PRINT_LOG( MAKE_LOGMSG( "ERROR: BAD_ALLOC: can't allocate for "#pointer ) );\
                {at_exit_code}                                                              \
            }                                                                               \
        } while( 0 )

#define CHECK_DATA_FUNCS( data_funcs, at_exit_code )                                                \
        do                                                                                          \
        {                                                                                           \
            if( data_funcs.print_func == nullptr )                                                  \
                PRINT_LOG( MAKE_LOGMSG( "Warning: data_funcs has no print_func") );                 \
            if( data_funcs.comparator == nullptr )                                                  \
                PRINT_LOG( MAKE_LOGMSG( "Warning: data_funcs has no comparator") );                 \
            if( data_funcs.copy_func == nullptr )                                                   \
            {                                                                                       \
                PRINT_LOG( MAKE_LOGMSG( "ERROR: BAD_DATA_FUNCS: data_funcs has no copy func") );    \
                {at_exit_code}                                                                      \
            }                                                                                       \
            if( data_funcs.destructor == nullptr )                                                  \
            {                                                                                       \
                PRINT_LOG( MAKE_LOGMSG( "ERROR: BAD_DATA_FUNCS: data_funcs has no destructor") );   \
                {at_exit_code}                                                                      \
            }                                                                                       \
        } while( 0 )

#endif //__CCONT_H__
