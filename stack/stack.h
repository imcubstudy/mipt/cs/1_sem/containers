#ifndef __STACK_H__
#define __STACK_H__

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "ccont.h"

#define STK_DEFAULT_CAPACITY 10

#define MAKE_STACK( name, type, data_funcs, poison_value, def_capacity )                                \
    stack_t* name = construct_stack( #name, sizeof( type ), data_funcs, &(poison_value), def_capacity );

namespace vl_containers
{
    struct stack_t
    {
        char*           _name;

        size_t          _default_capacity;
        size_t          _size;
        size_t          _capacity;

        void**          _root;

        size_t          _data_sizeof;
        data_funcs_t    _data_funcs;
        void*           _poison_data;
    };

    stack_t*        construct_stack( const char* name, size_t data_sizeof, data_funcs_t data_funcs,
                                    const void* poison_data, size_t default_capacity );
    int             destruct_stack( stack_t* stack );
    stack_t*        copy_stack( const stack_t* src );

    int             dump_stack( FILE* fstream, const stack_t* stack );

    cont_error_t    stk_push(     stack_t* stack, const void* src );
    cont_error_t    stk_pop(      stack_t* stack, void* dest );
    cont_error_t    stk_top(      stack_t* stack, void* dest );

    size_t          stk_size(     stack_t* stack );
    size_t          stk_capacity( stack_t* stack );
    bool            stk_ok(       stack_t* stack );

    cont_error_t    stk_clear(    stack_t* stack );

    bool            stk_empty(    stack_t* stack );

    cont_error_t    stk_resize(   stack_t* stack, size_t new_capacity );
}

#endif // __STACK_H__
