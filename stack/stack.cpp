#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "stack.h"

#define DAE( stack, returnp )                   \
    do                                          \
    {                                           \
        destruct_stack( stack );                \
        LOG_ON_END;                             \
        return returnp;                         \
    } while( 0 )

#ifndef __$$$ACHTUNG_DISABLE_VERIFIER$$$__

    #define STACK_OK( stack, at_exit_code )                                         \
        do                                                                          \
        {                                                                           \
            if( (stack) == nullptr )                                                \
            {                                                                       \
                PRINT_LOG( MAKE_LOGMSG( "STACK_OK FAILED: Stack is nullptr" ) );    \
                { at_exit_code }                                                    \
            }                                                                       \
            if( (stack)->_name == nullptr )                                         \
            {                                                                       \
                PRINT_LOG( MAKE_LOGMSG( "STACK_OK FAILED: Stack has undef name" ) );\
                { at_exit_code }                                                    \
            }                                                                       \
            if( (stack)->_root == nullptr )                                         \
            {                                                                       \
                NAMED_PRINT_LOG( "STACK_OK FAILED: root is nullptr", stack );       \
                { at_exit_code }                                                    \
            }                                                                       \
            if( (stack)->_default_capacity == 0 )                                   \
            {                                                                       \
                NAMED_PRINT_LOG( "STACK_OK FAILED: zero default capacity", stack ); \
                { at_exit_code }                                                    \
            }                                                                       \
            if( (stack)->_capacity == 0 )                                           \
            {                                                                       \
                NAMED_PRINT_LOG( "STACK_OK FAILED: zero capacity", stack );         \
                { at_exit_code }                                                    \
            }                                                                       \
            if( (stack)->_capacity < (stack->_size) )                               \
            {                                                                       \
                NAMED_PRINT_LOG( "STACK_OK FAILED: overflow", stack );              \
                { at_exit_code }                                                    \
            }                                                                       \
            CHECK_DATA_FUNCS( (stack)->_data_funcs,{                                \
                NAMED_PRINT_LOG( "STACK_OK FAILED: bad data funcs", stack );        \
                { at_exit_code }                                                    \
            });                                                                     \
            for( size_t i = 0; i < (stack)->_capacity; ++i )                        \
            {                                                                       \
                if( (stack)->_root[i] == nullptr )                                  \
                {                                                                   \
                    NAMED_PRINT_LOG( "STACK_OK FAILED: nullptr data pointer", stack );\
                    { at_exit_code }                                                \
                }                                                                   \
            }                                                                       \
        } while( 0 )

#else
    #define STACK_OK( stack, at_exit_code )                                     \
    do                                                                          \
    {                                                                           \
        if( (stack) == nullptr )                                                \
        {                                                                       \
            PRINT_LOG( MAKE_LOGMSG( "STACK_OK FAILED: Stack is nullptr" ) );    \
            { at_exit_code }                                                    \
        }                                                                       \
    } while(0)
#endif

namespace vl_containers
{
    stack_t* construct_stack( const char* name, size_t data_sizeof, data_funcs_t data_funcs, const void* poison_data, size_t default_capacity )
    {
        LOG_ON_START;

        PRECONDITION_P( name, {
            LOG_ON_END;
            return nullptr;
        });

        CHECK_DATA_FUNCS( data_funcs, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION( data_sizeof > 0, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( poison_data, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION( default_capacity > 0, {
            LOG_ON_END;
            return nullptr;
        });

        stack_t* stack = (stack_t*)calloc( sizeof( stack_t ), 1 );
        POST_ALLOC_CHECK( stack, {
            LOG_ON_END;
            return nullptr;
        });

        stack->_name = (char*)calloc( sizeof( char ), strlen( name ) + 1 );
        POST_ALLOC_CHECK( stack->_name, { DAE( stack, nullptr ); });
        strcpy( stack->_name, name );

        stack->_data_funcs  = data_funcs;

        stack->_data_sizeof = data_sizeof;

        stack->_capacity            = default_capacity;
        stack->_default_capacity    = default_capacity;

        stack->_size = 0;

        stack->_poison_data = calloc( stack->_data_sizeof, 1 );
        POST_ALLOC_CHECK( stack->_poison_data, { DAE( stack, nullptr ); });

        data_copy_func copy_func = stack->_data_funcs.copy_func;
        assert( copy_func );
        copy_func( stack->_poison_data, poison_data );

        stack->_root = (void**)calloc( sizeof( void* ), stack->_capacity );
        POST_ALLOC_CHECK( stack->_root, { DAE( stack, nullptr ); });

        for( size_t i = 0; i < stack->_capacity; ++i )
        {
            void* data_p = calloc( stack->_data_sizeof, 1 );
            CONDITION_P( data_p, "ERROR: BAD_ALLOC( can't construct root )", { DAE( stack, nullptr ); });

            stack->_root[i] = data_p;
            copy_func( stack->_root[i], stack->_poison_data );
        }

        STACK_OK( stack, { DAE( stack, nullptr ); });

        LOG_ON_END;
        return stack;
    }

    stack_t* copy_stack( const stack_t* src )
    {
        LOG_ON_START;

        PRECONDITION_P( src, {
            LOG_ON_END;
            return nullptr;
        });

        STACK_OK( src, {
            LOG_ON_END;
            return nullptr;
        });

        cont_error_t func_err = NO_ERROR;

        stack_t* stack = (stack_t*)calloc( sizeof( stack_t ), 1 );
        POST_ALLOC_CHECK( stack, {
            LOG_ON_END;
            return nullptr;
        });

        stack->_name = (char*)calloc( sizeof( char ), strlen( src->_name ) + strlen( "_copy" ) + 1 );
        POST_ALLOC_CHECK( stack->_name, { DAE( stack, nullptr ); });
        strcpy( stack->_name, src->_name );
        strcat( stack->_name, "_copy" );

        stack->_data_sizeof = src->_data_sizeof;

        stack->_poison_data = calloc( stack->_data_sizeof, 1 );
        POST_ALLOC_CHECK( stack->_poison_data, { DAE( stack, nullptr ); });

        stack->_data_funcs       = src->_data_funcs;
        data_copy_func copy_func = src->_data_funcs.copy_func;
        assert( copy_func );
        copy_func( stack->_poison_data, src->_poison_data );

        stack->_default_capacity = src->_default_capacity;

        stack->_capacity    = src->_capacity;
        stack->_size        = src->_size;

        stack->_root        = (void**)calloc( sizeof( void* ), stack->_capacity );
        POST_ALLOC_CHECK( stack->_root, { DAE( stack, nullptr ); });

        for( size_t i = 0; i < stack->_capacity; ++i )
        {
            void* data_p = calloc( stack->_data_sizeof, 1 );
            CONDITION_P( data_p, "ERROR: BAD_ALLOC( can't construct root )", { DAE( stack, nullptr ); });

            stack->_root[i] = data_p;
            copy_func( stack->_root[i], src->_root[i] );
        }

        STACK_OK( stack, { DAE( stack, nullptr ); });

        LOG_ON_END;
        return stack;
    }

    int destruct_stack( stack_t* stack )
    {
        LOG_ON_START;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return -1;
        });

        if( stack->_root != nullptr )
        {
            data_destructor destructor = stack->_data_funcs.destructor;
            assert( destructor );

            for( size_t i = 0; i < stack->_capacity; ++i )
                if( stack->_root[i] != nullptr )
                {
                    destructor( stack->_root[i] );
                    free( stack->_root[i] );
                    stack->_root[i] = nullptr;
                }

            free( stack->_root );
            stack->_root = nullptr;
        }

        if( stack->_poison_data != nullptr )
        {
            data_destructor destructor = stack->_data_funcs.destructor;
            assert( destructor );
            destructor( stack->_poison_data );

            free( stack->_poison_data );
            stack->_poison_data = nullptr;
        }

        if( stack->_name != nullptr )
        {
            free( stack->_name );
            stack->_name = nullptr;
        }

        int result  = (int)(stack->_size % INT_MAX);
        free( stack );

        LOG_ON_END;
        return result;
    }

    size_t stk_size( stack_t* stack )
    {
        LOG_ON_START;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return 0;
        });
        STACK_OK( stack, {
            LOG_ON_END;
            return 0;
        });

        LOG_ON_END;
        return stack->_size;
    }

    bool stk_empty( stack_t* stack )
    {
        LOG_ON_START;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return true;
        });

        STACK_OK( stack, {
            LOG_ON_END;
            return true;
        });

        LOG_ON_END;
        return stack->_size == 0;
    }

    size_t stk_capacity( stack_t* stack )
    {
        LOG_ON_START;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return 0;
        });
        STACK_OK( stack, {
            LOG_ON_END;
            return 0;
        });

        LOG_ON_END;
        return stack->_capacity;
    }

    cont_error_t stk_clear( stack_t* stack )
    {
        LOG_ON_START;

        cont_error_t func_err = NO_ERROR;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });
        STACK_OK( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        void** new_root = (void**)calloc( sizeof( void* ), stack->_capacity );
        POST_ALLOC_CHECK( new_root, {
            LOG_ON_END;
            return BAD_ALLOC;
        });

        data_copy_func copy_func = stack->_data_funcs.copy_func;
        assert( copy_func );

        for( size_t i = 0; i < stack->_capacity; ++i )
        {
            void* data_p = calloc( stack->_data_sizeof, 1 );
            CONDITION_P( data_p, "ERROR: BAD_ALLOC ( can't realloc root )", {
                data_destructor destructor = stack->_data_funcs.destructor;
                assert( destructor );

                for( size_t j = 0; j < i; j++ )
                {
                    destructor( new_root[j] );
                    free( new_root[j] );
                    new_root[j] = nullptr;
                }

                free( new_root );
                new_root = nullptr;

                LOG_ON_END;
                return BAD_ALLOC;
            });

            new_root[i] = data_p;
            copy_func( new_root[i], stack->_poison_data );
        }

        data_destructor destructor = stack->_data_funcs.destructor;
        assert( destructor );

        for( size_t i = 0; i < stack->_capacity; ++i )
            if( stack->_root[i] != nullptr )
            {
                destructor( stack->_root[i] );
                free( stack->_root[i] );
                stack->_root[i] = nullptr;
            }

        free( stack->_root );
        stack->_root = nullptr;

        stack->_capacity    = stack->_default_capacity;
        stack->_root        = new_root;

        LOG_ON_END;
        return func_err;
    }

    cont_error_t stk_resize( stack_t* stack, size_t new_capacity )
    {
        LOG_ON_START;

        cont_error_t func_err = NO_ERROR;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION( new_capacity > 0, {
            LOG_ON_END;
            return BAD_ARG;
        });

        STACK_OK( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        if( new_capacity < stack->_size )
        {
            PRINT_LOG( "This action will cause loss of data" );
            func_err = LOSS_OF_DATA;
        }

        void** new_root = (void**)calloc( sizeof( void* ), new_capacity );
        POST_ALLOC_CHECK( new_root, {
            LOG_ON_END;
            return BAD_ALLOC;
        });

        data_copy_func copy_func = stack->_data_funcs.copy_func;
        assert( copy_func );

        for( size_t i = 0; i < new_capacity; ++i )
        {
            void* data_p = calloc( stack->_data_sizeof, 1 );
            CONDITION_P( data_p, "ERROR: BAD_ALLOC ( can't realloc root )", {
                data_destructor destructor = stack->_data_funcs.destructor;
                assert( destructor );

                for( size_t j = 0; j < i; j++ )
                {
                    destructor( new_root[j] );
                    free( new_root[j] );
                    new_root[j] = nullptr;
                }
                free( new_root );
                new_root = nullptr;

                LOG_ON_END;
                return STK_OVERFLOW;
            });

            new_root[i] = data_p;
            if( i < stack->_capacity )
                copy_func( new_root[i], stack->_root[i] );
            else
                copy_func( new_root[i], stack->_poison_data );
        }

        data_destructor destructor = stack->_data_funcs.destructor;
        assert( destructor );

        for( size_t i = 0; i < stack->_capacity; ++i )
            if( stack->_root[i] != nullptr )
            {
                destructor( stack->_root[i] );
                free( stack->_root[i] );
                stack->_root[i] = nullptr;
            }

        free( stack->_root );
        stack->_root = nullptr;

        stack->_capacity    = new_capacity;
        stack->_root        = new_root;

        if( new_capacity < stack->_size )
            stack->_size = new_capacity;

        LOG_ON_END;
        return func_err;
    }

    cont_error_t stk_push( stack_t* stack, const void* src )
    {
        LOG_ON_START;

        cont_error_t func_err = NO_ERROR;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( src, {
            LOG_ON_END;
            return BAD_ARG;
        });

        STACK_OK( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        if( stack->_size >= stack->_capacity )
        {
            func_err = stk_resize( stack, stack->_capacity * 2 );
            CONDITION( func_err == NO_ERROR, "ERROR: STACK_OVERFLOW", {
                LOG_ON_END;
                return func_err;
            });
        }

        size_t insertion_pos = stack->_size;

        data_copy_func copy_func = stack->_data_funcs.copy_func;
        assert( copy_func );

        copy_func( stack->_root[insertion_pos], src );

        stack->_size += 1;

        LOG_ON_END;
        return func_err;
    }

    cont_error_t stk_pop( stack_t* stack, void* dest )
    {
        LOG_ON_START;
        cont_error_t func_err = NO_ERROR;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( dest, {
            LOG_ON_END;
            return BAD_ARG;
        });

        STACK_OK( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        data_copy_func copy_func = stack->_data_funcs.copy_func;
        assert( copy_func );

        CONDITION( stack->_size > 0, "Warning: empty_pop, returning poison", {
            func_err = EMPTY_POP;

            copy_func( dest, stack->_poison_data );

            LOG_ON_END;
            return func_err;
        });

        if( stack->_size - 1 > stack->_default_capacity &&
            stack->_size - 1 < stack->_capacity / 2 )
        {
            func_err = stk_resize( stack, stack->_capacity / 2 );
            CONDITION( func_err == NO_ERROR, "Can't stretch root??", {
                assert( "Wut? Look your log" );
            } );
        }

        size_t getting_pos = stack->_size - 1;

        copy_func( dest, stack->_root[getting_pos] );
        copy_func( stack->_root[getting_pos], stack->_poison_data );

        stack->_size -= 1;

        LOG_ON_END;
        return func_err;
    }

    cont_error_t stk_top( stack_t* stack, void* dest )
    {
        LOG_ON_START;

        cont_error_t func_err = NO_ERROR;

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( dest, {
            LOG_ON_END;
            return BAD_ARG;
        });

        STACK_OK( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        data_copy_func copy_func = stack->_data_funcs.copy_func;
        assert( copy_func );

        CONDITION( stack->_size > 0, "Warning: empty_pop, returning poison", {
            func_err = EMPTY_POP;

            copy_func( dest, stack->_poison_data );

            LOG_ON_END;
            return func_err;
        });

        size_t getting_pos = stack->_size - 1;

        copy_func( dest, stack->_root[getting_pos] );

        LOG_ON_END;
        return func_err;
    }

    int dump_stack( FILE* fstream, const stack_t* stack )
    {
        LOG_ON_START;

        PRECONDITION_P( fstream, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( stack, {
            LOG_ON_END;
            return BAD_ARG;
        });

        fprintf( fstream, "Stack_t %s[%p]\n",
                 stack->_name != nullptr ? stack->_name : "with undef name", stack );
        fprintf( fstream, "{\n" );
        fprintf( fstream,
                 "\troot             = [%p]\n"
                 "\tsize             = %zd\n"
                 "\tcapacity         = %zd\n"
                 "\tdata_sizeof      = %zd\n"
                 "\tdefault capacity = %zd\n",
                 stack->_root,
                 stack->_size,
                 stack->_capacity,
                 stack->_data_sizeof,
                 stack->_default_capacity );
        fprintf( fstream, "\tpoison data      = ");

        data_print_func print_func = stack->_data_funcs.print_func;
        PRECONDITION_P( print_func, {
            print_func = undef_type_print;
        });

        print_func( fstream, stack->_poison_data );

        fprintf( fstream, "\n" );

        if( stack->_root != nullptr )
        {
            fprintf( fstream, "\n\tELEM LIST:\n\n" );
            for( size_t i = 0; i < stack->_capacity; ++i )
            {
                fprintf( fstream, "\t%c[%zd] = ", i < stack->_size ? '+' : '-', i );

                print_func( fstream, stack->_root[i] );

                fprintf( fstream, "\n" );
            }
        }

        fprintf( fstream, "}\n" );

        LOG_ON_END;
        return 0;
    }

    bool            stk_ok( stack_t* stack )
    {
        LOG_ON_START;
        STACK_OK( stack, {
            LOG_ON_END;
            return false;
        });

        LOG_ON_END;
        return true;
    }
}

#undef DAE
#undef STACK_OK
