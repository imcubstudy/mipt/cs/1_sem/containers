#ifndef __LIST_H__
#define __LIST_H__

#include "ccont.h"

#define LST_DEFAULT_CAPACITY 10

#define MAKE_LIST( name, type, data_funcs, poison_value, def_capacity )\
    list_t* name = construct_list( #name, sizeof( type ), data_funcs, &(poison_value), def_capacity );

namespace vl_containers
{
    struct list_t
    {
        char*               _name;

        typedef struct list_elem_t list_elem_t;
        list_elem_t*        _data;

        size_t              _default_capacity;
        size_t              _capacity;
        size_t              _size;

        size_t              _data_sizeof;
        data_funcs_t        _data_funcs;
        void*               _poison_data;

        int                 _head;
        int                 _tail;
        int                 _free;
    };


    list_t*		    construct_list( const char* name, size_t data_sizeof, data_funcs_t data_funcs, const void* poison_data, size_t default_capacity );
    list_t*         copy_list( const list_t* src );
    int             destruct_list( list_t* list );

    int             dump_list( 	 FILE* ofstream, list_t* list );
    int             dot_list(  	 FILE* ofstream, list_t* list );
    int				subdot_list( FILE* ofstream, list_t* list, const char* label );

    cont_error_t	lst_push_h(    list_t* list, const void* value, int* inserted_pos );
    cont_error_t    lst_push_t(    list_t* list, const void* value, int* inserted_pos );
    cont_error_t    lst_insert(    list_t* list, int position, const void* value, int* inserted_pos );

    cont_error_t 	lst_pop_h(     list_t* list, void* dest, int* new_head );
    cont_error_t	lst_pop_t(     list_t* list, void* dest, int* new_tail );
    cont_error_t	lst_get_elem(  list_t* list, void* dest, int position );

    cont_error_t	lst_erase(     list_t* list, int position );
    cont_error_t    lst_clear(     list_t* list );

    cont_error_t    lst_resize(    list_t* list, size_t new_capacity );
    cont_error_t    lst_normalize( list_t* list );

    int		        lst_get_h(     list_t* list );
    int		        lst_get_t(     list_t* list );

    size_t          lst_size(      list_t* list );
    bool            lst_empty(     list_t* list );
    bool            lst_ok(        list_t* list );

    int		        find_by_value( list_t* list, const void* value );
    int             find_by_ln(    list_t* list, int ln );
}

#endif // __LIST_H__
