#include <cassert>
#include <cstring>
#include <cstdlib>
#include "list.h"

#ifndef LIST_DEFAULT_CAPACITY$
    #define LIST_DEFAULT_CAPACITY$ 10
#endif // LIST_DEFAULT_CAPACITY$

namespace vl_containers
{
    struct list_elem_t
    {
        void*       _data;

        int         _next;
        int         _prev;
    };

#define __$cmp_elems( a, b )    \
    ((a)._data == (b)._data && (a)._next == (b)._next && (a)._prev == (b)._prev)

#define DAE( list, returnp )                        \
    do                                              \
    {                                               \
        destruct_list( list );                      \
        LOG_ON_END;                                 \
        return returnp;                             \
    } while( 0 )

#define ELEM_OK( list, elem, at_exit_code )                                                     \
    do                                                                                          \
    {                                                                                           \
        if( elem._data == nullptr )                                                             \
        {                                                                                       \
            PRINT_LOG( "LIST_ELEM_OK FAILED: data is nullptr" );                                \
            { at_exit_code }                                                                    \
        }                                                                                       \
        if( elem._next != -1 )                                                                  \
        {                                                                                       \
            if( elem._next >= 0 && elem._next < list->_capacity )                               \
            {                                                                                   \
                if( list->_data[elem._next]._prev != -1 )                                       \
                    if( !( __$cmp_elems( list->_data[list->_data[elem._next]._prev], elem ) ) ) \
                    {                                                                           \
                        PRINT_LOG( "LIST_ELEM_OK FAILED: next link check failed" );             \
                        { at_exit_code }                                                        \
                    }                                                                           \
            }                                                                                   \
            else                                                                                \
            {                                                                                   \
                PRINT_LOG( "LIST_ELEM_OK FAILED: bad next link" );                              \
                { at_exit_code }                                                                \
            }                                                                                   \
        }                                                                                       \
        if( elem._prev != -1 )                                                                  \
        {                                                                                       \
            if( elem._prev >= 0 && elem._prev < list->_capacity )                               \
            {                                                                                   \
                if( list->_data[elem._prev]._prev != -1 )                                       \
                    if( !( __$cmp_elems( list->_data[list->_data[elem._prev]._next], elem ) ) ) \
                    {                                                                           \
                        PRINT_LOG( "LIST_ELEM_OK FAILED: prev link check failed" );             \
                        { at_exit_code }                                                        \
                    }                                                                           \
            }                                                                                   \
            else                                                                                \
            {                                                                                   \
                PRINT_LOG( "LIST_ELEM_OK FAILED: bad prev link" );                              \
                { at_exit_code }                                                                \
            }                                                                                   \
        }                                                                                       \
    } while( 0 )
    
#ifndef __$$$ACHTUNG_DISABLE_VERIFIER$$$__
    
    #define LIST_OK( list, at_exit_code )                                                           \
        do                                                                                          \
        {                                                                                           \
            if( (list) == nullptr )                                                                 \
            {                                                                                       \
                PRINT_LOG( "LIST_OK FAILED: list is nullptr" );                                     \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( (list)->_name == nullptr )                                                          \
            {                                                                                       \
                PRINT_LOG( "LIST_OK FAILED: list has undef name nullptr" );                         \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( (list)->_data == nullptr )                                                          \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has nullptr data array", list );             \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( (list)->_poison_data == nullptr )                                                   \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has nullptr poison data", list );            \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_default_capacity > 0) )                                                  \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has null default capacity", list );          \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_capacity > 0) )                                                          \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has null capacity", list );                  \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_size >= 0 && (list)->_size < (list)->_capacity) )                        \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has corrupted size", list );                 \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_data_sizeof > 0) )                                                       \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has null data sizeof", list );               \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_head >= 0 && (list)->_head < (list)->_capacity) )                        \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has corrupted head", list );                 \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_tail >= 0 && (list)->_tail < (list)->_capacity) )                        \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has tail corrupted", list );                 \
                { at_exit_code }                                                                    \
            }                                                                                       \
            if( !((list)->_free >= 0 && (list)->_free < (list)->_capacity) )                        \
            {                                                                                       \
                NAMED_PRINT_LOG( "LIST_OK FAILED: list has corrupted free", list );                 \
                { at_exit_code }                                                                    \
            }                                                                                       \
            for( int i = 0; i < (list)->_capacity; ++i )                                            \
                ELEM_OK( list, list->_data[i], {                                                    \
                NAMED_PRINT_LOG( "LIST_OK FAILED: bad elem found", list );                          \
                { at_exit_code }                                                                    \
            } );                                                                                    \
            CHECK_DATA_FUNCS( (list)->_data_funcs, {                                                \
                NAMED_PRINT_LOG( "LIST_OK FAILED: bad data funcs", list );                          \
                { at_exit_code }                                                                    \
            });                                                                                     \
        } while( 0 )
    
#else
    #define LIST_OK( list, at_exit_code )                                                       \
    do                                                                                          \
    {                                                                                           \
        if( (list) == nullptr )                                                                 \
        {                                                                                       \
            PRINT_LOG( "LIST_OK FAILED: list is nullptr" );                                     \
            { at_exit_code }                                                                    \
        }                                                                                       \
    } while(0)
#endif
    
    int             dump_list( FILE* ofstream, list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( ofstream, { LOG_ON_END; return 0; });
        PRECONDITION_P( list,     { LOG_ON_END; return 0; });

        fprintf( ofstream, "digraph %s\n"
                "{\n"
                "\t rankdir=LR\n"
                "\t edge [ color=black ]\n"
                "\t node [ shape=record ]\n\n", list->_name );

        fprintf( ofstream, "\tInfoNode [label=\""
                           "name \\n %s | "
                           "data \\n %p | "
                           "data_sizeof \\n %zd | "
                           "poison_data \\n %p | "
                           "capacity \\n %zd | "
                           "size \\n %zd | "
                           "<head> head \\n %d | "
                           "<tail> tail \\n %d | "
                           "<free> free \\n %d "
                           "\", "
                           "color=black]",
                           list->_name,
                           list->_data,
                           list->_data_sizeof,
                           list->_poison_data,
                           list->_capacity,
                           list->_size,
                           list->_head,
                           list->_tail,
                           list->_free );

        data_print_func print_func = list->_data_funcs.print_func;
        if( print_func == nullptr ) print_func = undef_type_print;

        for( int i = 0; i < list->_capacity; ++i )
        {
            fprintf( ofstream, "\tNode%d [label=\"<physn> Physical number \\n %d | { <prev> prev \\n %d | value \\n", i, i, list->_data[i]._prev );
            print_func( ofstream, list->_data[i]._data );
            fprintf( ofstream, " | <next> next \\n %d }\", color=%s]\n", list->_data[i]._next, list->_data[i]._prev == -1 ? "black" : "blue" );
        }

        for( int i = list->_head, j = 0; i != list->_free && j < MAX_LIST_LINKS_DUMPS$; j++ )
        {
            if( i == -1 ) break;
            int next = list->_data[i]._next;

            if( next != -1 ) fprintf( ofstream, "\tNode%d:<next> -> Node%d:<physn> [color=green]\n", i, next );
            i = next;
        }

        for( int i = list->_tail, j = 0; i != list->_free && j < MAX_LIST_LINKS_DUMPS$; j++ )
        {
            if( i == -1 ) break;
            int prev = list->_data[i]._prev;

            if( prev != -1 ) fprintf( ofstream, "\tNode%d:<prev> -> Node%d:<physn> [color=black]\n", i, prev );
            i = prev;
        }

        fprintf( ofstream, "\tInfoNode:<head> -> Node%d [dir=both, color=blue]\n"
                           "\tInfoNode:<tail> -> Node%d [dir=both, color=blue]\n"
                           "\tInfoNode:<free> -> Node%d [dir=both, color=blue]\n",
                           list -> _head, list -> _tail, list -> _free );

        fprintf( ofstream, "\n}\n\n" );
        fflush( ofstream );

        LOG_ON_END;
        return 1;
    }

    int             dot_list( FILE* ofstream, list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( ofstream, { LOG_ON_END; return 0; });
        PRECONDITION_P( list,     { LOG_ON_END; return 0; });
        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        } );

        assert( lst_normalize( list ) == NO_ERROR );

        fprintf( ofstream, "digraph %s\n"
                "{\n"
                "\t rankdir=LR\n"
                "\t edge [ color=black ]\n"
                "\t node [ shape=record ]\n\n", list->_name );

        fprintf( ofstream, "\tInfoNode [label=\""
                           "name \\n %s | "
                           "poison_data \\n %p | "
                           "size \\n %zd | "
                           "\", "
                           "color=black]\n",
                           list->_name,
                           list->_poison_data,
                           list->_size );

        data_print_func print_func = list->_data_funcs.print_func;
        if( print_func == nullptr ) print_func = undef_type_print;
        
        for( int i = 0; i < list->_size; ++i )
        {
            fprintf( ofstream, "\tNode%d [label=\"<physn> Physical number \\n %d | { <prev> prev \\n %d | value \\n", i, i, list->_data[i]._prev );
            print_func( ofstream, list->_data[i]._data );
            fprintf( ofstream, " | <next> next \\n %d }\", color=%s]\n", list->_data[i]._next, list->_data[i]._prev == -1 ? "black" : "blue" );
        }

        for( int i = 0; i < list->_size; i++ )
        {
            int next = list->_data[i]._next;

            if( next != -1 ) fprintf( ofstream, "\tNode%d:<next> -> Node%d:<physn> [color=green]\n", i, next );
        }

        for( int i = 0; i < list->_size; i++ )
        {
            int prev = list->_data[i]._prev;

            if( prev != -1 ) fprintf( ofstream, "\tNode%d:<prev> -> Node%d:<physn> [color=black]\n", i, prev );
        }

        fprintf( ofstream, "\n}\n\n" );
        fflush( ofstream );

        LOG_ON_END;
        return 1;
    }

    int             subdot_list( FILE* ofstream, list_t* list, const char* label )
    {
        LOG_ON_START;

        PRECONDITION_P( ofstream, { LOG_ON_END; return 0; });
        PRECONDITION_P( list,     { LOG_ON_END; return 0; });
        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        } );

        assert( lst_normalize( list ) == NO_ERROR );

        fprintf( ofstream, "\nsubgraph cluster_%s\n"
                "{\n"
                "\t label=\"%s\"\n"
                "\t rankdir=LR\n"
                "\t edge [ color=black ]\n"
                "\t node [ shape=record ]\n\n", label, list->_name );

        fprintf( ofstream, "\tInfoNode%p [label=\""
                           "name \\n %s | "
                           "poison_data \\n %p | "
                           "size \\n %zd"
                           "\", "
                           "color=black]\n",
                           list,
                           list->_name,
                           list->_poison_data,
                           list->_size );

        data_print_func print_func = list->_data_funcs.print_func;
        if( print_func == nullptr ) print_func = undef_type_print;
        
        for( int i = 0; i < list->_size; ++i )
        {
            fprintf( ofstream, "\tNode%p [label=\"<physa> Physical address \\n %p | { <prev> prev \\n %p | value \\n", list->_data + i, list->_data + i,
                                                                            list->_data + list->_data[i]._prev );
            print_func( ofstream, list->_data[i]._data );
            fprintf( ofstream, " | <next> next \\n %p }\", color=%s]\n", list->_data + list->_data[i]._next, list->_data[i]._prev == -1 ? "black" : "blue" );
        }

        for( int i = 0; i < list->_size - 1; i++ )
        {
            int next = list->_data[i]._next;

            if( next != -1 ) fprintf( ofstream, "\tNode%p:<next> -> Node%p:<physa> [color=green]\n", list->_data + i, list->_data + next );
        }

        for( int i = 1; i < list->_size; i++ )
        {
            int prev = list->_data[i]._prev;

            if( prev != -1 ) fprintf( ofstream, "\tNode%p:<prev> -> Node%p:<physa> [color=black]\n", list->_data + i, list->_data + prev );
        }

        fprintf( ofstream, "InfoNode%p -> Node%p:<physa>\n", list, list->_data );

        fprintf( ofstream, "\n}\n\n" );
        fflush( ofstream );

        LOG_ON_END;
        return 1;
    }

    size_t          lst_size( list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return 0;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        });

        LOG_ON_END;
        return list->_size;
    }

    bool            lst_empty(    list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return true;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG");
            LOG_ON_END;
            return true;
        });

        return lst_size( list ) == 0;
    }

    cont_error_t    lst_clear( list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        list->_head = 0;
        list->_tail = 0;
        list->_free = 0;

        data_destructor destructor = list->_data_funcs.destructor;
        assert( destructor );

        data_copy_func  copy_func  = list->_data_funcs.copy_func;
        assert( copy_func );

        for( int i = 0; i < list->_capacity; ++i )
        {
            list->_data[i]._next = i + 1;
            list->_data[i]._prev = -1;

            destructor( list->_data[i]._data );
            copy_func(  list->_data[i]._data, list->_poison_data );
        }

        list->_data[list->_capacity - 1]._next = -1;

        list->_size = 0;

        LOG_ON_END;
        return NO_ERROR;
    }

    cont_error_t	lst_push_h( list_t* list, const void* value, int* inserted_pos )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( value, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( inserted_pos, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        if( list->_data[list->_free]._next == -1 )
        {
            cont_error_t resize_err = lst_resize( list, list -> _capacity * 2 );
            CONDITION( resize_err == NO_ERROR, "ERROR: Can't resize list", {
                LOG_ON_END;
                return resize_err;
            });
        }

        list_elem_t prev_head = list->_data[list->_head]; int prev_head_index = list->_head;
        list_elem_t prev_tail = list->_data[list->_tail]; int prev_tail_index = list->_tail;
        list_elem_t prev_free = list->_data[list->_free]; int prev_free_index = list->_free;

        list->_head                     = prev_free_index;
        list->_data[list->_head]._prev  = prev_free._next;

        data_destructor destructor = list->_data_funcs.destructor;
        assert( destructor );

        data_copy_func  copy_func  = list->_data_funcs.copy_func;
        assert( copy_func );

        destructor( list->_data[list->_head]._data );
        copy_func(  list->_data[list->_head]._data, value );

        if( prev_head_index == prev_free_index )
            list->_data[list->_head]._next                = prev_free._next;
        else
        {
            list->_data[list->_head]    ._next            = prev_head_index;
            list->_data[prev_head_index]._prev            = list -> _head;
        }

        list->_data[prev_tail_index]._next     = prev_free._next;
        list->_free                            = prev_free._next;

        list->_size++;

        *inserted_pos = list->_head;

        LOG_ON_END;
        return NO_ERROR;
    }

    cont_error_t    lst_push_t( list_t* list, const void* value, int* inserted_pos )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( value, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( inserted_pos, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        if( list->_data[list->_free]._next == -1 )
        {
            cont_error_t resize_err = lst_resize( list, list -> _capacity * 2 );
            CONDITION( resize_err == NO_ERROR, "ERROR: Can't resize list", {
                LOG_ON_END;
                return resize_err;
            });
        }

        list_elem_t prev_head = list->_data[list->_head]; int prev_head_index = list->_head;
        list_elem_t prev_tail = list->_data[list->_tail]; int prev_tail_index = list->_tail;
        list_elem_t prev_free = list->_data[list->_free]; int prev_free_index = list->_free;

        list->_tail                      = prev_free_index;
        list->_data[list->_tail]._next   = prev_free._next;

        data_destructor destructor = list->_data_funcs.destructor;
        assert( destructor );

        data_copy_func  copy_func  = list->_data_funcs.copy_func;
        assert( copy_func );

        destructor( list->_data[list->_tail]._data );
        copy_func(  list->_data[list->_tail]._data, value );

        if( prev_tail_index == prev_free_index )
            list->_data[list->_tail]._prev   = prev_free._next;
        else
        {
            list->_data[list->_tail]    ._prev   = prev_tail_index;
            list->_data[prev_tail_index]._next   = list->_tail;
        }

        list->_size++;

        list->_data[prev_head_index]._prev   = prev_free._next;
        list->_free                          = prev_free._next;

        *inserted_pos = list->_tail;

        LOG_ON_END;
        return NO_ERROR;
    }

    cont_error_t    lst_insert( list_t* list, int position, const void* value, int* inserted_pos )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( value, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        if( position != list -> _tail )
        {
            if( list->_data[list->_free]._next == -1 )
            {
                cont_error_t resize_err = lst_resize( list, list -> _capacity * 2 );
                CONDITION( resize_err == NO_ERROR, "ERROR: Can't resize list", {
                    LOG_ON_END;
                    return resize_err;
                });
            }

            list_elem_t prev_free       = list->_data[list->_free];
            int         insertion_pos   = list->_free;

            data_destructor destructor  = list->_data_funcs.destructor;
            assert( destructor );

            data_copy_func  copy_func   = list->_data_funcs.copy_func;
            assert( copy_func );

            destructor( list->_data[insertion_pos]._data );
            copy_func(  list->_data[insertion_pos]._data, value );

            list->_data[insertion_pos]._prev = position;
            list->_data[insertion_pos]._next = list->_data[position]._next;

            list->_data[list->_data[position]._next]._prev  = insertion_pos;
            list->_data[position]._next                     = insertion_pos;

            list->_data[list->_tail]._next = prev_free._next;
            list->_data[list->_head]._prev = prev_free._next;

            list->_size++;

            list->_free = prev_free._next;

            *inserted_pos = insertion_pos;

            LOG_ON_END;
            return NO_ERROR;
        }
        else if( position == list->_tail )
        {
            cont_error_t push_back_result = lst_push_t( list, value, inserted_pos );

            CONDITION( push_back_result == NO_ERROR, "ERROR: Can't do lst_push_t", {
                LOG_ON_END;
                return push_back_result;
            });

            LOG_ON_END;
            return push_back_result;
        }
        return cont_error_t::CRIT;
    }

    cont_error_t 	lst_pop_h( list_t* list, void* dest, int* new_head )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( dest, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( new_head, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        list_elem_t prev_head = list->_data[list->_head]; int prev_head_index = list->_head;
        list_elem_t prev_tail = list->_data[list->_tail]; int prev_tail_index = list->_tail;
        list_elem_t prev_free = list->_data[list->_free]; int prev_free_index = list->_free;

        list->_head                             = prev_head._next;
        list->_data[prev_head_index]._next      = prev_free_index;
        list->_data[prev_head_index]._prev      = -1;

        data_destructor destructor  = list->_data_funcs.destructor;
        assert( destructor );

        data_copy_func  copy_func   = list->_data_funcs.copy_func;
        assert( copy_func );

        copy_func(  dest, list->_data[prev_head_index]._data );
        destructor( list->_data[prev_head_index]._data );
        copy_func( list->_data[prev_head_index]._data, list->_poison_data );

        list->_free                     = prev_head_index;

        list->_data[list->_head]._prev  = list->_free;
        list->_data[list->_tail]._next  = list->_free;

        list->_size--;

        *new_head = list->_head;

        LOG_ON_END;
        return NO_ERROR;
    }

    cont_error_t	lst_pop_t( list_t* list, void* dest, int* new_tail )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( dest, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( new_tail, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        list_elem_t prev_head = list->_data[list->_head]; int prev_head_index = list->_head;
        list_elem_t prev_tail = list->_data[list->_tail]; int prev_tail_index = list->_tail;
        list_elem_t prev_free = list->_data[list->_free]; int prev_free_index = list->_free;

        list->_tail                             = prev_tail._prev;
        list->_data[prev_tail_index]._next      = prev_free_index;
        list->_data[prev_tail_index]._prev      = -1;

        data_destructor destructor  = list->_data_funcs.destructor;
        assert( destructor );

        data_copy_func  copy_func   = list->_data_funcs.copy_func;
        assert( copy_func );

        copy_func(  dest, list->_data[prev_tail_index]._data );
        destructor( list->_data[prev_tail_index]._data );
        copy_func( list->_data[prev_tail_index]._data, list->_poison_data );

        list->_free                     = prev_tail_index;

        list->_data[list->_head]._prev  = list->_free;
        list->_data[list->_tail]._next  = list->_free;

        *new_tail = list->_tail;

        list->_size--;

        LOG_ON_END;
        return NO_ERROR;
    }

    cont_error_t	lst_get_elem( list_t* list, void* dest, int position )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION_P( dest, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION( position >= 0 && position <= list->_capacity, {
            LOG_ON_END;
            return BAD_ARG;
        });

        data_copy_func  copy_func   = list->_data_funcs.copy_func;
        assert( copy_func );

        copy_func( dest, list->_data[position]._data );

        LOG_ON_END;
        return NO_ERROR;
    }

    cont_error_t	lst_erase( list_t* list, int position )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });


        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        PRECONDITION( position >= 0 && position <= list->_capacity
                     && list->_data[position]._prev != -1,
        {
            LOG_ON_END;
            return BAD_ARG;
        });

        if( position != list->_head && position != list->_tail )
        {
            list->_data[list->_tail]._next = position;
            list->_data[list->_head]._next = position;

            list_elem_t removing = list->_data[position];

            list->_data[position]._prev = -1;
            list->_data[position]._next = list -> _free;
            list -> _free = position;

            list->_data[removing._prev]._next = removing._next;
            list->_data[removing._next]._prev = removing._prev;

            data_destructor destructor = list->_data_funcs.destructor;
            assert( destructor );

            data_copy_func copy_func = list->_data_funcs.copy_func;
            assert( copy_func );

            destructor( list->_data[position]._data );
            copy_func( list->_data[position]._data, list->_poison_data );

            list->_size--;

        }
        else if( position == list -> _head )
        {
            void* dest          = calloc( list->_data_sizeof, 1 );
            int new_head        = 0;
            cont_error_t res    = lst_pop_h( list, dest,  &new_head );

            data_destructor destructor = list->_data_funcs.destructor;
            assert( destructor );

            destructor( dest );
            free( dest );

            return res;
        }
        else if( position == list -> _tail )
        {
            void* dest          = calloc( list->_data_sizeof, 1 );
            int new_head        = 0;
            cont_error_t res    = lst_pop_t( list, dest,  &new_head );

            data_destructor destructor = list->_data_funcs.destructor;
            assert( destructor );

            destructor( dest );
            free( dest );

            return res;
        }
        return cont_error_t::CRIT;
    }

    int		        lst_get_h( list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return -1;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return -1;
        });

        LOG_ON_END;
        return list->_head;
    }

    int		        lst_get_t( list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return -1;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return -1;
        });

        LOG_ON_END;
        return list->_tail;
    }

    int		        find_by_value( list_t* list, const void* value )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return -1;
        });

        PRECONDITION_P( value, {
            LOG_ON_END;
            return -1;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return -1;
        });

        data_comparator comparator = list->_data_funcs.comparator;
        assert( comparator );

        for( int i = 0; i < list->_size; ++i )
        {
            if( comparator( value, list->_data[i]._data ) == 0 )
            {
                LOG_ON_END;
                return i;
            }
        }

        LOG_ON_END;
        return -1;
    }

    int             find_by_ln(    list_t* list, int ln )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return -1;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return -1;
        });

        PRECONDITION( ln > 0 && ln < list->_size, {
            LOG_ON_END;
            return -1;
        });

        for( int i = list->_head; i != list->_free; i = list->_data[i]._next )
        {
            if( i == ln )
            {
                LOG_ON_END;
                return i;
            }
        }

        PRINT_LOG( "Something strange is going on.." );
        LOG_ON_END;
        return -1;
    }

    list_t*		    construct_list( const char* name, size_t data_sizeof, data_funcs_t data_funcs, const void* poison_data, size_t default_capacity )
    {
        LOG_ON_START;

        PRECONDITION_P( name, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( poison_data, {
            LOG_ON_END;
            return nullptr;
        });

        CHECK_DATA_FUNCS( data_funcs, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION( data_sizeof > 0, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION( default_capacity > 0, {
            LOG_ON_END;
            return nullptr;
        });

        list_t* list = (list_t*)calloc( sizeof( list_t ), 1 );
        POST_ALLOC_CHECK( list, {
            LOG_ON_END;
            return nullptr;
        });

        list->_name = (char*)calloc( sizeof( char ), strlen( name ) + 1 );
        POST_ALLOC_CHECK( list->_name, { DAE( list, nullptr ); } );
        strcpy( list->_name, name );

        list->_data_sizeof = data_sizeof;
        list->_data_funcs  = data_funcs;

        list->_poison_data = calloc( list->_data_sizeof, 1 );
        POST_ALLOC_CHECK( list->_name, { DAE( list, nullptr ); } );

        data_copy_func copy_func = list->_data_funcs.copy_func;
        assert( copy_func );

        copy_func( list->_poison_data, poison_data );


        list->_default_capacity = default_capacity;
        list->_capacity         = list->_default_capacity;

        list->_data = (list_elem_t*)calloc( sizeof( list_elem_t ), list->_capacity );
        POST_ALLOC_CHECK( list->_data, { DAE( list, nullptr ); } );

        for( int i = 0; i < list->_capacity; ++i )
        {
            list->_data[i]._data    = calloc( list->_data_sizeof, 1 );
            POST_ALLOC_CHECK( list->_data[i]._data, { DAE( list, nullptr ); } );

            copy_func( list->_data[i]._data, list->_poison_data );

            list->_data[i]._next = i + 1;
            list->_data[i]._prev = -1;
        }
        list->_data[list->_capacity - 1]._next = -1;

        list->_size = 0;

        list->_head = 0;
        list->_tail = 0;
        list->_free = 0;

        LIST_OK( list, { DAE( list, nullptr ); } );
        LOG_ON_END;
        return list;
    }

    int             destruct_list( list_t* list )
    {
        LOG_ON_START;
        PRECONDITION_P( list, { return 0; } );

        if( list->_name != nullptr )
        {
            free( list->_name );
            list->_name = nullptr;
        }

        data_destructor destructor = list->_data_funcs.destructor;
        assert( destructor );

        if( list->_poison_data != nullptr )
        {
            destructor( list->_poison_data );
            free( list->_poison_data );
            list->_poison_data = nullptr;
        }

        if( list->_data != nullptr )
        {
            for( int i = 0; i < list->_capacity; ++i )
            {
                if( list->_data[i]._data != nullptr )
                {
                    destructor( list->_data[i]._data );
                    free( list->_data[i]._data );
                    list->_data[i]._data = nullptr;
                }
            }

            free( list->_data );
            list->_data = nullptr;
        }

        int res = (int)list->_size;
        free( list );

        LOG_ON_END;
        return res;
    }

    list_t*         copy_list( const list_t* src )
    {
        LOG_ON_START;

        PRECONDITION_P( src, {
            LOG_ON_END;
            return nullptr;
        });

        LIST_OK( src, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return nullptr;
        });

        list_t* res     = (list_t*)calloc( sizeof( list_t ), 1 );
        POST_ALLOC_CHECK( res, {
            LOG_ON_END;
            return nullptr;
        });

        res->_name = (char*)calloc( sizeof( char ), strlen( src->_name ) + strlen( "_copy" ) + 1 );
        POST_ALLOC_CHECK( res->_name, { DAE( res, nullptr ); } );
        strcpy( res->_name, src->_name );
        strcat( res->_name, "_copy" );

        res->_default_capacity   = src->_default_capacity;
        res->_capacity           = src->_capacity;
        res->_size               = src->_size;

        res->_head               = src->_head;
        res->_tail               = src->_tail;
        res->_free               = src->_free;

        res->_data_funcs         = src->_data_funcs;
        res->_data_sizeof        = src->_data_sizeof;

        data_copy_func copy_func = res->_data_funcs.copy_func;
        assert( copy_func );

        res->_poison_data        = calloc( res->_data_sizeof, 1 );
        POST_ALLOC_CHECK( res->_poison_data, { DAE( res, nullptr ); } );
        copy_func( res->_poison_data, src->_poison_data );

        res->_data               = (list_elem_t*)calloc( sizeof( list_elem_t ), res->_capacity );
        POST_ALLOC_CHECK( res->_data, { DAE( res, nullptr ); } );

        for( int i = 0; i < res->_capacity; ++i )
        {
            res->_data[i]._data = calloc( res->_data_sizeof, 1 );
            POST_ALLOC_CHECK( res->_data[i]._data, { DAE( res, nullptr ); } );

            copy_func( res->_data[i]._data, src->_data[i]._data );
            res->_data[i]._next = src->_data[i]._next;
            res->_data[i]._prev = src->_data[i]._prev;
        }

        LIST_OK( res, { DAE( res, nullptr ); } );

        LOG_ON_END;
        return res;
    }

    cont_error_t    lst_resize(   list_t* list, size_t new_capacity )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        cont_error_t func_err = NO_ERROR;

        CONDITION( new_capacity >= list->_size, "ERROR: BAD ARG", {
            func_err = BAD_ARG;
            LOG_ON_END;
            return func_err;
        });

        cont_error_t normalize_result = lst_normalize( list );
        CONDITION( normalize_result == NO_ERROR, "ERROR: Can't squeeze data array", {
            LOG_ON_END;
            return normalize_result;
        });

        list_elem_t*    new_data = (list_elem_t*)calloc( sizeof( list_elem_t ), new_capacity );
        POST_ALLOC_CHECK( new_data, {
            LOG_ON_END;
            return BAD_ALLOC;
        });

        data_copy_func copy_func = list->_data_funcs.copy_func;
        assert( copy_func );

        for( int i = 0; i < new_capacity; ++i )
        {
            new_data[i]._data   = calloc( list->_data_sizeof, 1 );
            POST_ALLOC_CHECK( new_data[i]._data, {
                data_destructor destructor = list->_data_funcs.destructor;
                assert( destructor );

                for( int j = 0; j < i; ++j )
                {
                    if( new_data[j]._data != nullptr )
                    {
                        destructor( new_data[j]._data );
                        free( new_data[j]._data );
                        new_data[j]._data = nullptr;
                    }
                }

                free( new_data );

                LOG_ON_END;
                return BAD_ALLOC;
            });

            if( i < list->_capacity )
            {
                copy_func( new_data[i]._data, list->_data[i]._data );
                new_data[i]._next = list->_data[i]._next;
                new_data[i]._prev = list->_data[i]._prev;
            }
            else
            {
                copy_func( new_data[i]._data, list->_poison_data );
                new_data[i]._next = i + 1;
                new_data[i]._prev = -1;
            }

        }

        new_data[new_capacity - 1]._next = -1;

        data_destructor destructor = list->_data_funcs.destructor;
        assert( destructor );

        for( int i = 0; i < list->_capacity; ++i )
        {
            destructor( list->_data[i]._data );
            free( list->_data[i]._data );
            list->_data[i]._data = nullptr;
        }
        free( list->_data );

        list->_data     = new_data;
        list->_data[list->_free]._next = list->_capacity;
        list->_capacity = new_capacity;

        LIST_OK( list, {
            LOG_ON_END;
            return  CRIT;
        });

        LOG_ON_END;
        return func_err;
    }

    cont_error_t    lst_normalize( list_t* list )
    {
        LOG_ON_START;

        PRECONDITION_P( list, {
            LOG_ON_END;
            return BAD_ARG;
        });

        LIST_OK( list, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return BAD_ARG;
        });

        if( list->_size == 0 )
        {
            LOG_ON_END;
            return NO_ERROR;
        }

        list_elem_t*    new_data = (list_elem_t*)calloc( sizeof( list_elem_t ), list->_capacity );
        POST_ALLOC_CHECK( new_data, {
            LOG_ON_END;
            return BAD_ALLOC;
        });

        data_copy_func copy_func = list->_data_funcs.copy_func;
        assert( copy_func );

        for( int i = 0, j = list->_head; i < list->_size; ++i, j = list->_data[j]._next )
        {
            new_data[i]._data = calloc( list->_data_sizeof, 1 );
            POST_ALLOC_CHECK( new_data[i]._data, {
                data_destructor destructor = list->_data_funcs.destructor;
                assert( destructor );

                for( int k = 0; k < i; ++k )
                {
                    if( new_data[k]._data != nullptr )
                    {
                        destructor( new_data[k]._data );
                        free( new_data[k]._data );
                        new_data[k]._data = nullptr;
                    }
                }

                free( new_data );

                LOG_ON_END;
                return BAD_ALLOC;
            });

            copy_func( new_data[i]._data, list->_data[j]._data );
        }

        for( int i = list->_size; i < list->_capacity; ++i )
        {
            new_data[i]._data = calloc( list->_data_sizeof, 1 );
            POST_ALLOC_CHECK( new_data[i]._data, {
                data_destructor destructor = list->_data_funcs.destructor;
                assert( destructor );

                for( int k = 0; k < i; ++k )
                {
                    if( new_data[k]._data != nullptr )
                    {
                        destructor( new_data[k]._data );
                        free( new_data[k]._data );
                        new_data[k]._data = nullptr;
                    }
                }

                free( new_data );

                LOG_ON_END;
                return BAD_ALLOC;
            });

            copy_func( new_data[i]._data, list->_poison_data );
        }

        for( int i = 0; i < list->_size; ++i )
        {
            new_data[i]._next = i + 1;
            new_data[i]._prev = ( i == 0 ? list->_size : i - 1 );
        }

        for( int i = list->_size; i < list->_capacity; ++i )
        {
            new_data[i]._next = ( (i == list->_capacity - 1) ? -1 : i + 1 );
            new_data[i]._prev = -1;
        }

        data_destructor destructor = list->_data_funcs.destructor;
        assert( destructor );

        for( int i = 0; i < list->_capacity; ++i )
        {
            destructor( list->_data[i]._data );
            free( list->_data[i]._data );
            list->_data[i]._data = nullptr;
        }
        free( list->_data );

        list->_data     = new_data;

        list->_head     = 0;
        list->_tail     = list->_size - 1;
        list->_free     = list->_size;

        LIST_OK( list, {
            LOG_ON_END;
            return CRIT;
        });

        LOG_ON_END;
        return NO_ERROR;
    }

    bool            lst_ok( list_t* list )
    {
        LOG_ON_START;

        LIST_OK( list, {
            LOG_ON_END;
            return false;
        });

        LOG_ON_END;
        return true;
    }
}

#undef ELEM_OK
#undef LIST_OK
#undef DAE

