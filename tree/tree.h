#ifndef __TREE_H__
#define __TREE_H__

#include <stdio.h>
#include "ccont.h"

#define MAKE_TREE( name, type, data_funcs, poison_value )\
    tree_t* name = construct_tree( #name, sizeof( type ), data_funcs, &(poison_value) );
namespace vl_containers
{
    struct node_t
    {
        //LOL, it works
        typedef struct tree_t tree_t;

        tree_t*             _tree;

        node_t*             parent;

        void*               data;

        node_t*             left;
        node_t*             right;
    };

    struct tree_t
    {
        char*               _name;

        node_t*             root;

        size_t              _data_sizeof;
        data_funcs_t        _data_funcs;
        void*               _poison_data;
    };

    node_t*     construct_node( tree_t* tree, void* data );
    node_t*     construct_node( tree_t* tree, void* data, node_t* left, node_t* right );
    int         destruct_node(  node_t* node );

    node_t*     copy_node( node_t* node, tree_t* tree );

    int         change_node_tree( node_t* node, tree_t* tree );
    int         change_node_data( node_t* node, void* new_data );

    size_t      count_node_childs( node_t* node );

    int         set_left ( node_t* node, node_t* left );
    int         set_right( node_t* node, node_t* right );

    tree_t*     construct_tree( const char* name, size_t data_sizeof, data_funcs_t data_funcs, const void* poison_data );
    int         destruct_tree( tree_t* tree );
    int         set_root( tree_t* tree, node_t* root_node );

    size_t      tree_size( tree_t* tree );
    bool        tree_ok( tree_t* tree );

    tree_t*     copy_tree( tree_t* src );
    int         dump_tree( FILE* fstream, tree_t* tree );
    int         dot_tree( FILE* fstream, tree_t* tree );
    int 		dot_node( FILE*	fstream, node_t* node );

    node_t*     find_by_value( node_t* root, void* const value );
    node_t*     find_leave_by_value( node_t* root, void* const value );
}
#endif /* __TREE_H__ */
