#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tree.h"
#include "ccont.h"

#define MAX_TREE_SIZE$ (1<<31)

#define DNAE( node, returnp )   \
    do                          \
    {                           \
        destruct_node( node );  \
        LOG_ON_END;             \
        return returnp;         \
    } while( 0 )

#define DTAE( tree, returnp )   \
    do                          \
    {                           \
        destruct_tree( tree );  \
        LOG_ON_END;             \
        return returnp;         \
    } while( 0 )

#define NODE_OK( node, at_exit_code )                                                       \
    do											                                            \
    {											                                            \
        if( (node) == nullptr )								                                \
        {											                                        \
            PRINT_LOG( MAKE_LOGMSG( "NODE_OK FAILED: Node is nullptr" ) );			        \
            {at_exit_code}						                                            \
        }											                                        \
        if( (node)->data == nullptr )							                            \
        {											                                        \
            PRINT_LOG( MAKE_LOGMSG( "NODE_OK FAILED: nullptr data pointer" ) );		        \
            {at_exit_code}						                                            \
        }											                                        \
        if( (node)->left != nullptr )							                            \
        {											                                        \
            if( (node)->left->parent != (node)                                              \
                && (node)->left->_tree == (node)->_tree )						            \
            {										                                        \
                PRINT_LOG( MAKE_LOGMSG( "NODE_OK FAILED: bad left link" ) );		        \
                {at_exit_code}						                                        \
            }										                                        \
        }											                                        \
        if( (node)->right != nullptr )							                            \
        {											                                        \
            if( (node)->right->parent != (node)  						                    \
                && (node)->left->_tree == (node)->_tree )						            \
            {										                                        \
                PRINT_LOG( MAKE_LOGMSG( "NODE_OK FAILED: bad right link" ) );	    	    \
                {at_exit_code}					                                            \
            }										                                        \
        }											                                        \
        if( (node)->parent != nullptr )							                            \
        {											                                        \
            if( !( (node)->parent->left == (node) || (node)->parent->right == (node) ) 	    \
                && (node)->parent->_tree == (node)->_tree )						            \
            {										                                        \
                PRINT_LOG( MAKE_LOGMSG( "NODE_OK FAILED: bad parent link" ) );  		    \
                {at_exit_code}					                                            \
            }										                                        \
        }											                                        \
    } while( 0 )
namespace vl_containers
{
    void $check_nodes( node_t* node, int* count, bool* broken )
    {
        *count += 1;
        if( *count > MAX_TREE_SIZE$ ) *broken = true;
        if( *broken ) return;

        if( node == nullptr ) return;
        
        NODE_OK( node, { *broken = true; } );
        
        $check_nodes( node->left, count, broken );
        $check_nodes( node->right, count, broken );
    }

#ifndef __$$$ACHTUNG_DISABLE_VERIFIER$$$__
    
    #define TREE_OK( tree, at_exit_code )									                    \
        do									                                                    \
        {										                                    	        \
            if( (tree) == nullptr )							                        	        \
            {										                                	        \
                PRINT_LOG( MAKE_LOGMSG( "TREE_OK FAILED: tree is nullptr" ) );        	        \
                {at_exit_code}						                                            \
            }											                                        \
            if( (tree)->_name == nullptr )                                                      \
            {                                                                                   \
                PRINT_LOG( MAKE_LOGMSG( "TREE_OK FAILED: undef name") );                        \
                {at_exit_code}                                                                  \
            }   										                                        \
            CHECK_DATA_FUNCS( (tree)->_data_funcs, {                                            \
                NAMED_PRINT_LOG( "TREE_OK FAILED: bad data funcs", tree );                      \
                {at_exit_code}                                                                  \
            });  		                                                                        \
            if( (tree)->_poison_data == nullptr )						                        \
            {											                                        \
                NAMED_PRINT_LOG( "TREE_OK FAILED: bad poison data", tree );                     \
                {at_exit_code}						                                            \
            }                                                                                   \
            bool nodes_ok = true;                                                               \
            int count = 0;                                                                      \
            $check_nodes( (tree)->root, &count, &nodes_ok );                                    \
            if( !nodes_ok )                                                                     \
            {											                                        \
                NAMED_PRINT_LOG( "TREE_OK FAILED: corrupted nodes", tree );                     \
                {at_exit_code}						                                            \
            }				                                                                    \
        } while( 0 )

#else
    #define TREE_OK( tree, at_exit_code )                                                        \
        do                                                                                       \
        {                                                                                        \
            if( (tree) == nullptr )                                                              \
            {                                                                                    \
                PRINT_LOG( MAKE_LOGMSG( "TREE_OK FAILED: tree is nullptr" ) );                   \
                {at_exit_code}                                                                   \
            }                                                                                    \
        } while(0)
#endif
    
    node_t* construct_node( tree_t* tree, void* data )
    {
        LOG_ON_START;

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return nullptr;
        });

        TREE_OK( tree, {
            LOG_ON_END;
            return nullptr;
        });

        node_t* node    = (node_t*)calloc( sizeof( node_t ), 1 );
        POST_ALLOC_CHECK( node, {
            LOG_ON_END;
            return nullptr;
        });

        node->_tree     = tree;
        node->parent    = nullptr;
        node->left      = nullptr;
        node->right     = nullptr;

        node->data      = calloc( tree->_data_sizeof, 1 );
        POST_ALLOC_CHECK( node->data, { DNAE( node, nullptr ); } );

        data_copy_func copy_func = tree->_data_funcs.copy_func;
        assert( copy_func );

        copy_func( node->data, data );

        NODE_OK( node, { DNAE( node, nullptr ); } );

        LOG_ON_END;
        return node;
    }

    node_t* construct_node( tree_t* tree, void* data, node_t* left, node_t* right )
    {
        LOG_ON_START;

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( data, {
            LOG_ON_END;
            return nullptr;
        });

        TREE_OK( tree, {
            LOG_ON_END;
            return nullptr;
        });

        node_t* node = construct_node( tree, data );
        CONDITION_P( node, "ERROR: Can't construct node", {
            LOG_ON_END;
            return nullptr;
        });

        set_left( node, left );
        set_right( node, right );

        NODE_OK( node, { DNAE( node, nullptr ); });

        LOG_ON_END;
        return node;
    }

    int destruct_node( node_t* node )
    {
        LOG_ON_START;

        if( node == nullptr )
        {
            LOG_ON_END;
            return 0;
        }

        if( node->data != nullptr )
        {
            data_destructor destructor = node->_tree->_data_funcs.destructor;
            assert( destructor );
            destructor( node->data );

            free( node->data );
            node->data = nullptr;
        }

        node->data  = nullptr;
        node->_tree = nullptr;

        int res = 0;

        if( node->left  != nullptr ) res += destruct_node( node->left );
        node->left = nullptr;

        if( node->right != nullptr ) res += destruct_node( node->right );
        node->right = nullptr;

        free( node );

        LOG_ON_END;
        return res + 1;
    }

    node_t* copy_node( node_t* node, tree_t* tree )
    {
        LOG_ON_START;

        PRECONDITION_P( node, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( tree, {
           LOG_ON_END;
            return nullptr;
        });

        TREE_OK( tree, {
            LOG_ON_END;
            return nullptr;
        });

        if( node == nullptr )
        {
            LOG_ON_END;
            return nullptr;
        }

        node_t* result = (node_t*)calloc( sizeof( node_t ), 1 );
        POST_ALLOC_CHECK( result, { DNAE( result, nullptr ); } );

        result->parent = nullptr;

        result->_tree = tree;

        result->data = calloc( node->_tree->_data_sizeof, 1 );
        POST_ALLOC_CHECK( result->data, { DNAE( result, nullptr ); } );

        data_copy_func copy_func = node->_tree->_data_funcs.copy_func;
        assert( copy_func );
        copy_func( result->data, node->data );

        if( node->left  != nullptr ) set_left ( result, copy_node( node->left,  tree  ) );
        if( node->right != nullptr ) set_right( result, copy_node( node->right, tree ) );

        NODE_OK( result, { DNAE( result, nullptr ); } );

        LOG_ON_END;
        return result;
    }

    int change_node_tree( node_t* node, tree_t* tree )
    {
        LOG_ON_START;

        PRECONDITION_P( node, {
            LOG_ON_END;
            return 0;
        });

        NODE_OK( node, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return 0;
        });

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return 0;
        });

        TREE_OK( tree, {
            PRINT_LOG( "ERROR: BAD ARG" );
            LOG_ON_END;
            return 0;
        });

        int res = 0;
        node->_tree = tree;

        if( node->left  != nullptr )
            res += change_node_tree( node->left,  tree );
        if( node->right != nullptr )
            res += change_node_tree( node->right, tree );

        LOG_ON_END;
        return res + 1;
    }

    int dump_node( FILE* fstream, node_t* node, int* dumps )
    {
        LOG_ON_START;

        if( node == nullptr )
        {
            LOG_ON_END;
            return 0;
        }

    #ifndef MAX_TREE_NODE_DUMPS$
        #define MAX_TREE_NODE_DUMPS$ 1024
    #endif

        if( (*dumps)++ > MAX_TREE_NODE_DUMPS$ )
        {
            PRINT_LOG( "Limit reached" );
            LOG_ON_END;
            return 0;
        }

        int res = 0;

        fprintf( fstream, "\tNode%p [label=\"{ { <pointer> %p | size \\n %zd | { data \\n %p | value \\n ", node, node, count_node_childs( node ), node->data );
        node->_tree->_data_funcs.print_func( fstream, node->data );
        fprintf( fstream, "} } | { <left> Left \\n %p | <right> Right \\n %p } }\"]\n", node->left, node->right );

        res += dump_node( fstream, node->left, dumps );
        if( node->left != nullptr )
            fprintf( fstream, "\tNode%p:<left> -> Node%p:<pointer>\n", node, node->left );

        res += dump_node( fstream, node->right, dumps );
        if( node->right != nullptr )
            fprintf( fstream, "\tNode%p:<right> -> Node%p:<pointer>\n", node, node->right );

        LOG_ON_END;
        return res + 1;
    }

    int dot_node( FILE* fstream, node_t* node )
    {
        LOG_ON_START;

        PRECONDITION_P( fstream, {
            LOG_ON_END;
            return 0;
        });

        if( node == nullptr )
        {
            LOG_ON_END;
            return 0;
        }

        int res = 0;

        fprintf( fstream, "\tNode%p [label=\"{ { data \\n | ", node );
        node->_tree->_data_funcs.print_func( fstream, node->data );
        fprintf( fstream, " } | { <left> Left | <right> Right \\n } }\"]\n" );

        res += dot_node( fstream, node->left );
        if( node->left != nullptr )
            fprintf( fstream, "\tNode%p:<left> -> Node%p\n", node, node->left );

        res += dot_node( fstream, node->right );
        if( node->right != nullptr )
            fprintf( fstream, "\tNode%p:<right> -> Node%p\n", node, node->right );

        LOG_ON_END;
        return res + 1;
    }

    int set_left( node_t* node, node_t* left )
    {
        LOG_ON_START;

        PRECONDITION_P( node, {
            LOG_ON_END;
            return 0;
        });

        NODE_OK( node, {
            LOG_ON_END;
            return 0;
        });

        CONDITION( node->left == nullptr, "Warning: destructing existing nodes", {
            destruct_node( node->left );
        });

        node->left = left;

        int res = 0;

        if( left != nullptr )
        {
            res += change_node_tree( left, node->_tree );
            left->parent = node;
        }

        LOG_ON_END;
        return res;
    }

    int set_right( node_t* node, node_t* right )
    {
        LOG_ON_START;

        PRECONDITION_P( node, {
            LOG_ON_END;
            return 0;
        });

        NODE_OK( node, {
            LOG_ON_END;
            return 0;
        });

        CONDITION( node->right == nullptr, "Warning: destructing existing node", {
            destruct_node( node->right );
        });

        node->right = right;

        int res = 0;

        if( right != nullptr )
        {
            right->parent = node;
            res += change_node_tree( right, node->_tree );
        }

        LOG_ON_END;
        return res;
    }

    tree_t* construct_tree( const char* name, size_t data_sizeof, data_funcs_t data_funcs, const void* poison_data )
    {
        LOG_ON_START;

        PRECONDITION_P( name,{
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION( data_sizeof > 0, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( poison_data, {
            LOG_ON_END;
            return nullptr;
        });

        CHECK_DATA_FUNCS( data_funcs, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return nullptr;
        });

        tree_t* tree = (tree_t*)calloc( sizeof( tree_t ), 1 );
        POST_ALLOC_CHECK( tree, {
            LOG_ON_END;
            return nullptr;
        });

        tree->_name = (char*)calloc( sizeof( char ), strlen( name ) + 1 );
        POST_ALLOC_CHECK( tree->_name, { DTAE( tree, nullptr ); });
        strcpy( tree->_name, name );

        tree->_data_sizeof = data_sizeof;
        tree->_data_funcs  = data_funcs;

        tree->_poison_data = calloc( tree->_data_sizeof, 1 );
        POST_ALLOC_CHECK( tree->_poison_data, { DTAE( tree, nullptr ); } );

        data_copy_func copy_func = tree->_data_funcs.copy_func;
        assert( copy_func );

        copy_func( tree->_poison_data, poison_data );

        TREE_OK( tree, { DTAE( tree, nullptr ); } );

        LOG_ON_END;
        return tree;
    }

    int destruct_tree( tree_t* tree )
    {
        LOG_ON_START;

        if( tree == nullptr )
        {
            LOG_ON_END;
            return 0;
        }

        if( tree->_poison_data )
        {
            data_destructor destructor = tree->_data_funcs.destructor;
            assert( destructor );

            destructor( tree->_poison_data );

            free( tree->_poison_data );
            tree->_poison_data = nullptr;
        }
        if( tree->_name )
        {
            free( tree->_name );
            tree->_name = nullptr;
        }

        if( tree->_poison_data ) { free( tree->_poison_data ); tree->_poison_data = nullptr; }
        if( tree->_name )        { free( tree->_name );        tree->_name        = nullptr; }

        int res = destruct_node( tree->root );

        free( tree );

        LOG_ON_END;
        return res;
    }

    tree_t* copy_tree( tree_t* src )
    {
        LOG_ON_START;

        PRECONDITION_P( src, {
            LOG_ON_END;
            return nullptr;
        });

        TREE_OK( src, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return nullptr;
        });

        char* _name = (char*)calloc( sizeof( char ), strlen( src->_name ) + strlen( "_copy" ) + 1 );
        POST_ALLOC_CHECK( _name, {
            LOG_ON_END;
            return nullptr;
        });
        strcpy( _name, src->_name );
        strcat( _name, "_copy" );

        tree_t* result = construct_tree( _name,
                                         src->_data_sizeof,
                                         src->_data_funcs,
                                         src->_poison_data );

        CONDITION_P( result, "ERROR: Can't construct tree", {
            free( _name );
            LOG_ON_END;
            return nullptr;
        });

        result->root = copy_node( src->root, result );

        TREE_OK( result, DTAE( result, nullptr ); );

        LOG_ON_END;
        return result;
    }

    int dump_tree( FILE* fstream, tree_t* tree )
    {
        LOG_ON_START;

        PRECONDITION_P( fstream, {
            LOG_ON_END;
            return 0;
        });

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return 0;
        });

        fprintf( fstream,
                 "digraph tree%p\n"
                         "{\n"
                         "\tnode [shape=record, color=blue]\n"
                         "\trankdir=TB\n", tree );

        fprintf( fstream, "\tInfoNode [label=\"{ size \\n %zd | <root> root \\n %p}\"]\n", tree_size( tree ), tree->root );

        int node_dumps = 0;
        int res = dump_node( fstream, tree->root, &node_dumps );

        if( tree->root != nullptr )
            fprintf( fstream, "\tInfoNode:<root> -> Node%p:<pointer>\n", tree->root );

        fprintf( fstream, "}\n" );

        LOG_ON_END;
        return res;
    }

    int dot_tree( FILE* fstream, tree_t* tree )
    {
        LOG_ON_START;

        PRECONDITION_P( fstream, {
            LOG_ON_END;
            return 0;
        });

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return 0;
        });

        TREE_OK( tree, {
            LOG_ON_END;
            return 0;
        });

        fprintf( fstream,
                 "digraph tree_t%p\n"
                         "{\n"
                         "\tnode [shape=record, color=blue]\n"
                         "\trankdir=TB\n", tree );

        fprintf( fstream, "\tInfoNode [label=\"{ name \\n | %s }\"]", tree->_name );

        int res = dot_node( fstream, tree->root );

        fprintf( fstream, "}\n" );

        LOG_ON_END;
        return res;
    }

    node_t* find_by_value( node_t* root, void* const value )
    {
        LOG_ON_START;

        PRECONDITION_P( root, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( value, {
            LOG_ON_END;
            return nullptr;
        });

        NODE_OK( root, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return nullptr;
        });

        data_comparator cmp = root->_tree->_data_funcs.comparator;
        assert( root->_tree->_data_funcs.comparator );

        if( cmp( root->data, value ) == 0 )
        {
            LOG_ON_END;
            return root;
        }
        else
        {
            node_t* result = nullptr;
            if( root->left != nullptr )
            {
                result = find_by_value( root->left, value );
                if( result != nullptr )
                {
                    LOG_ON_END;
                    return result;
                }
            }
            if( root->right != nullptr )
            {
                result = find_by_value( root->right, value );
                if( result != nullptr )
                {
                    LOG_ON_END;
                    return result;
                }
            }
            LOG_ON_END;
            return nullptr;
        }
        LOG_ON_END;
    }

    node_t*     find_leave_by_value( node_t* root, void* const value )
    {
        LOG_ON_START;

        PRECONDITION_P( root, {
            LOG_ON_END;
            return nullptr;
        });

        PRECONDITION_P( value, {
            LOG_ON_END;
            return nullptr;
        });

        NODE_OK( root, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return nullptr;
        });

        data_comparator cmp = root->_tree->_data_funcs.comparator;
        assert( root->_tree->_data_funcs.comparator );

        if( cmp( root->data, value ) == 0 && root->left == nullptr && root->right == nullptr )
        {
            LOG_ON_END;
            return root;
        }
        else
        {
            node_t* result = nullptr;
            if( root->left != nullptr )
            {
                result = find_leave_by_value( root->left, value );
                if( result != nullptr && result->left == nullptr && result->right == nullptr )
                {
                    LOG_ON_END;
                    return result;
                }
            }
            if( root->right != nullptr )
            {
                result = find_leave_by_value( root->right, value );
                if( result != nullptr && result->left == nullptr && result->right == nullptr )
                {
                    LOG_ON_END;
                    return result;
                }
            }
            LOG_ON_END;
            return nullptr;
        }
        LOG_ON_END;
    }

    int change_node_data( node_t* node, void* new_data )
    {
        LOG_ON_START;

        PRECONDITION_P( node, {
            LOG_ON_END;
            return 0;
        });

        PRECONDITION_P( new_data, {
            LOG_ON_END;
            return 0;
        });

        NODE_OK( node, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        });

        data_destructor destructor = node->_tree->_data_funcs.destructor;
        assert( destructor );

        destructor( node->data );

        data_copy_func copy_func = node->_tree->_data_funcs.copy_func;
        assert( copy_func );

        copy_func( node->data, new_data );

        LOG_ON_END;
        return 0;
    }

    size_t count_node_childs( node_t* node )
    {
        LOG_ON_START;

        if( node != nullptr )
        {
            NODE_OK( node, {
                PRINT_LOG( "ERROR: BAD_ARG" );
                LOG_ON_END;
                return 0;
            });

            size_t res = 0;

            res += count_node_childs( node->left );
            res += count_node_childs( node->right );

            LOG_ON_END;
            return res + 1;
        }

        LOG_ON_END;
        return 0;
    }

    size_t      tree_size( tree_t* tree )
    {
        LOG_ON_START;

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return 0;
        });

        TREE_OK( tree, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        });

        size_t res = count_node_childs( tree->root );

        LOG_ON_END;
        return res;
    }

    int set_root( tree_t* tree, node_t* root_node )
    {
        LOG_ON_START;

        PRECONDITION_P( tree, {
            LOG_ON_END;
            return 0;
        });

        PRECONDITION_P( root_node, {
            LOG_ON_END;
            return 0;
        });

        TREE_OK( tree, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        });

        NODE_OK( root_node, {
            PRINT_LOG( "ERROR: BAD_ARG" );
            LOG_ON_END;
            return 0;
        });

        if( tree->root != nullptr )
        {
            PRINT_LOG( MAKE_LOGMSG( "Warning: destructing existing nodes" ) );
            destruct_node( tree->root );
        }

        tree->root = root_node;

        int res = change_node_tree( root_node, tree );

        LOG_ON_END;
        return res;
    }

    bool        tree_ok( tree_t* tree )
    {
        LOG_ON_START;
        TREE_OK( tree, {
            LOG_ON_END;
            return false;
        });

        LOG_ON_END;
        return true;
    }
}

#undef DNAE
#undef DTAE
#undef NODE_OK
#undef TREE_OK
